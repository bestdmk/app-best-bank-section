$(document).ready(function () {
  function mediaSize() {
    if (window.matchMedia("(max-width: 999px)").matches) {
      $(".bbest-app-content-cards .slick-dots").css("display", "flex");
      $(".bbest-app-content-cards").removeClass("container");
      $(".bbest-app-content-cards").slick({
        dots: true,
        infinite: false,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 4,
        centerMode: true,
        variableWidth: true,
        autoplay: false,
        autoplaySpeed: 4000,
        arrows: false,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1,
              dots: true,
            },
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              infinite: false,
            },
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
            },
          },
        ],
      });
    } else {
      $(".bbest-app-content-cards").addClass("container");
      $(".bbest-app-content-cards .slick-dots").css("display", "none");
      $(".bbest-app-content-cards .slick-track").removeClass("slick-track");
      $(".bbest-app-content-cards .slick-track").addClass("container");
    }
  }
  window.addEventListener("resize", mediaSize, true);
  mediaSize();

  $(".round").click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    $(".arrow").toggleClass("bounceAlpha");
  });
});